<?php

/**
 * Custom Meta Box
 */
if(!function_exists('add_custom_meta_box'))
{
    function add_custom_meta_box()
    {
        add_meta_box(
            'custom_meta_box', // $id
            'Event Details', // $title
            'show_custom_meta_box', // $callback
            'event', // $post_type
            'normal', // $context
            'high' // $priority
        );
    }
}

/**
 * Show Custom Fields
 */
if(!function_exists('show_custom_meta_box'))
{
    function show_custom_meta_box()
    {
        global $custom_meta_fields, $post;

        $html = '<input type="hidden" name="custom_meta_box_nonce" value="' . wp_create_nonce(basename(__FILE__)) . '" />'. // use nonce for verification
                        '<table class="form-table">';
        foreach($custom_meta_fields as $field) {
            $html .=    '<tr>'.
                                '<th>'.
                                    '<label for="'.$field['id'].'">'.$field['label'].'</label>'.
                                '</th>'.
                                '<td>'.
                                    '<input '.
                                        'type="text" '.
                                        'name="'.$field['id'].'" '.
                                        'id="'.$field['id'].'" '.
                                        'value="'.get_post_meta($post->ID, $field['id'], true).'" '. // get value of this field if it exists for this post
                                        'size="30" '.
                                        'class="event-cpt-' . $field['type'] . '" '.
                                        (($field['id'] == 'event_tickets_text')?
                                            'placeholder="Defaults to \'Purchase Tickets\'" '
                                            :'').
                                        '/>'.
                                    '<br />'.
                                    '<span class="description">'.$field['desc'].'</span>'.
                                '</td>'.
                            '</tr>';
        }
        $html .= '</table>';

        echo $html;
    }
}

/**
 * Save Custom Field Data
 */
if(!function_exists('save_custom_meta'))
{
    function save_custom_meta($post_id)
    {
        global $custom_meta_fields;

        if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__))) { // verify nonce
            return $post_id;
        }

        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) { // check autosave
            return $post_id;
        }

        /* Check Permissions */
        if ('page' == $_POST['post_type']) {
            if (!current_user_can('edit_page', $post_id)) {
                return $post_id;
            } else if (!current_user_can('edit_post', $post_id)) {
                return $post_id;
            }
        }

        /* Loop Through Fields & Save Data */
        foreach ($custom_meta_fields as $field) {

            $old = get_post_meta($post_id, $field['id'], true);
            $new = $_POST[$field['id']];

            if ($new && $new != $old) {
                update_post_meta($post_id, $field['id'], $new);
            } elseif ('' == $new && $old) {
                delete_post_meta($post_id, $field['id'], $old);
            }
        }
    }
}

/**
 * Register Event CPT
 */
if(!function_exists('event_cpt'))
{
    function event_cpt()
    {
        $arrLabels = array(
            'name'                => _x('Events', 'Post Type General Name', 'text_domain'),
            'singular_name'       => _x('Event', 'Post Type Singular Name', 'text_domain'),
            'menu_name'           => __('Events', 'text_domain'),
            'parent_item_colon'   => __('Parent Event:', 'text_domain'),
            'all_items'           => __('All Events', 'text_domain'),
            'view_item'           => __('View Event', 'text_domain'),
            'add_new_item'        => __('Add New Event', 'text_domain'),
            'add_new'             => __('Add New', 'text_domain'),
            'edit_item'           => __('Edit Event', 'text_domain'),
            'update_item'         => __('Update Event', 'text_domain'),
            'search_items'        => __('Search Event', 'text_domain'),
            'not_found'           => __('Event not found', 'text_domain'),
            'not_found_in_trash'  => __('Event not found in Trash', 'text_domain'),
        );

        $arrArgs = array(
            'label'               => __('event', 'text_domain'),
            'description'         => __('Events will show up on the Tour page.', 'text_domain'),
            'labels'              => $arrLabels,
            'supports'            => array('title', 'editor',),
            'taxonomies'          => array(),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => plugins_url('/event-cpt/images/1day.png'),
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => true,
            'publicly_queryable'  => true,
            'rewrite'             => false,
            'capability_type'     => 'page',
        );

        register_post_type('event', $arrArgs); // register custom post type
    }
}

/**
 * Include Plugin CSS & JS
 */
if(!function_exists('event_cpt_scripts'))
{
    function event_cpt_scripts()
    {
        wp_register_style('event-cpt', plugins_url('/event-cpt/css/event-cpt.css'));
        wp_enqueue_style('event-cpt');
    }
}

/**
 *
 */
if(!function_exists('event_cpt_admin_scripts'))
{
    function event_cpt_admin_scripts()
    {
        wp_register_style('jquery-ui', plugins_url('/event-cpt/css/lib/jquery-ui-1.10.4.custom.min.css'));
        wp_enqueue_style('jquery-ui');

        wp_register_script('event-cpt', plugins_url('/js/event-cpt.js', __FILE__ ), array('jquery-ui-datepicker'));
        wp_enqueue_script('event-cpt');
    }
}

/**
 *  Generate <LI> HTML for Event Listing in Short Code Function
 */
if(!function_exists('event_cpt_event_listing'))
{
    function event_cpt_event_listing($data)
    {
        $html = "";

        $strPageURL = "http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI];

        // Array of month abbreviations (utility)
        $arrMonths = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');

        if(!$data['list']) {

            $arrEventDetails =& $data['event'];

            $boolVIP = true;
            $strTicketsVIP = $arrEventDetails['event_tickets_url_vip'][0];
            $strTickets = $arrEventDetails['event_tickets_url'][0];

            if ($strTicketsVIP != '') {
                if (strpos($strTicketsVIP, 'http://') === false){
                    $strTicketsVIP = 'http://' . $strTicketsVIP;
                }
            } else {
                $boolVIP = false;
            }

            if (strpos($strTickets, 'http://') === false){
                $strTickets = 'http://' . $strTickets;
            }

            $html .=    '<li class="show">'.
                                    '<div class="top row">'.
                                        '<div class="info">'.
                                            '<div class="col-md-2">'.
                                                '<span class="date">'.
                                                    '<span class="month">' . $arrMonths[date('n', strtotime($arrEventDetails['event_date'][0]))] . '</span>'.
                                                    '<span class="day">' . date('d', strtotime($arrEventDetails['event_date'][0])) . '</span>'.
                                                '</span>'.
                                            '</div>'.
                                            '<div class="col-md-4">'.
                                                '<ul class="details">'.
                                                    '<li class="location brand">' . $arrEventDetails['event_city'][0] . ', ' . $arrEventDetails['event_state'][0] . '</li>'.
                                                    '<li class="doors time">Doors open @ ' . $arrEventDetails['event_time_doors'][0] . '</li>'.
                                                    '<li class="start time">Show @ ' . $arrEventDetails['event_time_start'][0] . '</li>'.
                                                    '<li class="sharing">'.
                                                        '<!-- AddThis Button BEGIN -->'.
                                                        '<div class="addthis_toolbox addthis_default_style addthis_16x16_style">'.
                                                            '<a class="addthis_button_facebook" addthis:url="' . $strPageURL . '?id=' . $data['id'] . '"></a>'.
                                                            '<a class="addthis_button_twitter" addthis:url="' . $strPageURL . '?id=' . $data['id'] . '"></a>'.
                                                            '<a class="addthis_button_google_plusone_share" addthis:url="' . $strPageURL . '?id=' . $data['id'] . '"></a>'.
                                                            '<a class="addthis_button_compact" addthis:url="' . $strPageURL . '?id=' . $data['id'] . '"></a>'.
                                                            '<a class="addthis_counter addthis_bubble_style" addthis:url="' . $strPageURL . '?id=' . $data['id'] . '"></a>'.
                                                        '</div>'.
                                                        '<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>'.
                                                        '<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52ed57365b4ef019"></script>'.
                                                        '<!-- AddThis Button END -->'.
                                                    '</li>'.
                                                '</ul>'.
                                            '</div>'.
                                        '</div>'.
                                        '<div class="content col-md-6">'.
                                            (($intId != 0)?
                                                '<h3 class="brand">' . $data['title'] . '</h3>'
                                                :
                                                '<h3><a href="?id=' . $data['id'] . '" class="brand">' . $data['title'] . '</a></h3>'
                                                ).
                                            '<div class="desc">' . wp_trim_words($data['content'], 80) . '</div>'.
                                        '</div>'.
                                    '</div>'.
                                    '<div class="bottom row">'.
                                        '<ul>'.
                                            '<li class="facebook col-md-3 col-sm-6">'.
                                                '<a '.(($arrEventDetails['event_facebook_id'][0] != '')?'href="http://www.facebook.com/events/' . $arrEventDetails['event_facebook_id'][0] . '"':'class="inactive"').'>'.
                                                    'RSVP on <img src="' . plugins_url('/event-cpt/images/facebook.png') . '" alt="Facebook" />'.
                                                '</a>'.
                                            '</li>'.
                                            '<li class="invite col-md-3 col-sm-6">'.
                                                '<!-- AddThis Button BEGIN -->'.
                                                '<a href="#" class="addthis_button_email" addthis:url="' . $strPageURL . '?id=' . $data['id'] . '">Invite Friends</a>'.
                                                '<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>'.
                                                '<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52ed57365b4ef019"></script>'.
                                                '<!-- AddThis Button END -->'.
                                            '</li>'.
                                            '<li class="tickets-vip col-md-3 col-sm-6">'.
                                                '<a '.(($boolVIP)?'href="' . $strTicketsVIP . '" target="_blank"':'class="inactive"').'>VIP Tickets</a>'.
                                            '</li>'.
                                            '<li class="tickets-reg col-md-3 col-sm-6">'.
                                                '<a href="' . $strTickets . '" class="tickets" target="_blank">'.
                                                    (($arrEventDetails['event_tickets_text'][0]) ? $arrEventDetails['event_tickets_text'][0] : 'Purchase Tickets').
                                                '</a>'.
                                            '</li>'.
                                        '</ul>'.
                                    '</div>'.
                                '</li>';

        } else {

            $html .=    '<li class="show">'.
                                    '<span class="date">'.
                                        '<span class="month">' . $arrMonths[date('n', strtotime($arrEventDetails['event_date'][0]))] . '</span>'.
                                        '<span class="day">' . date('d', strtotime($arrEventDetails['event_date'][0])) . '</span>'.
                                    '</span>'.
                                    '<strong class="title">' . $data['title'] . '</strong>'.
                                    '<span class="location">' . $arrEventDetails['event_city'][0] . ', ' . $arrEventDetails['event_state'][0] . '</span>'.
                                '</li>';

        }

        return $html;
    }
}

/**
 * Shortcode
 */
if(!function_exists('event_cpt_shortcode'))
{
    function event_cpt_shortcode($atts)
    {
        // Collect Attributes
        $a = shortcode_atts(array(
            'limit' => -1,
            'list' => false,
        ), $atts);

        $intLimit = $a['limit'];
        $boolList = $a['list'];

        // Check for single
        $intId = $_GET['id'];

        if ($intId != 0) {
            $args = array('post_type' => 'event', 'p' => $intId);
        } else {
            $strToday = date('Y-m-d');
            $args = array(
                'post_type' => 'event',
                'posts_per_page' => $intLimit,
                'orderby' => 'meta_value',
                'meta_key' => 'event_date',
                'order' => 'ASC',
                'meta_query' => array(
                    array(
                        'key' => 'event_date',
                        'value' => $strToday,
                        'compare' => '>='
                    ),
                )
            );
        }

        $query = new WP_Query($args);

        $html = "";

        if($query->have_posts()) {
            $html .=    '<ul class="'.((!$boolList)?'shows container':'shows-list').'">';

            while ($query->have_posts()) : $query->the_post();

                $html .= event_cpt_event_listing(array(
                    'event'=>get_post_meta(get_the_ID()),
                    'id'=>get_the_ID(),
                    'title'=>get_the_title(),
                    'content'=>get_the_content(),
                    'list'=>$boolList
                ));

            endwhile;

            $html .=    '</ul>';
        }

        echo $html;
    }
}

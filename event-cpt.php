<?php
/*
Plugin Name: TWOSEVENTWO events
Version: 0.2.1
Description: Custom Post Type for Tour page.
Author: Jeff Tribble
Author URI: http://twoseventwo.us/
*/

function event_cpt_plugin_get_version() {
    $plugin_data = get_plugin_data( __FILE__ );
    $plugin_version = $plugin_data['Version'];
    return $plugin_version;
}

require 'plugin-updates/plugin-update-checker.php';

$MyUpdateChecker = new PluginUpdateChecker(
    'http://plugins.skorinc.com/updates/event-cpt/meta.json',
    __FILE__,
    'event-cpt',
    1
);

/* ******************************************************************* */

$prefix = 'event'; // name of post type

if(!defined('EVENT_CPT_DIR')) {
    define('EVENT_CPT_DIR',str_replace("\\","/",dirname( __FILE__ )));
}

/**
 * Custom Fields
 */
$custom_meta_fields = array(
    array(
        'label'    => 'Date*',
        'desc'     => 'The date (or start date) of the event.',
        'id'       => $prefix . '_date',
        'required' => true,
        'type'     => 'date'
    ),
    array(
        'label'    => 'City*',
        'desc'     => 'The city of the event.',
        'id'       => $prefix . '_city',
        'required' => true,
        'type'     => 'text'
    ),
    array(
        'label'    => 'State or Country (if outside U.S.)*',
        'desc'     => 'i.e. CA, TX, UK. If the event is located in the United States, this field represents the state. Otherwise, enter the country of the event.',
        'id'       => $prefix . '_state',
        'required' => true,
        'type'     => 'text'
    ),
    array(
        'label'    => 'Doors Time*',
        'desc'     => 'The doors time of the event. i.e. 6PM, 8:30PM, 11AM.',
        'id'       => $prefix . '_time_doors',
        'required' => true,
        'type'     => 'text'
    ),
    array(
        'label'    => 'Start Time*',
        'desc'     => 'The start time of the event. i.e. 6PM, 8:30PM, 11AM.',
        'id'       => $prefix . '_time_start',
        'required' => true,
        'type'     => 'text'
    ),
    array(
        'label'    => 'Tickets Text*',
        'desc'     => 'The text that shows up in the far right button.',
        'id'       => $prefix . '_tickets_text',
        'required' => true,
        'type'     => 'text'
    ),
    array(
        'label'    => 'Tickets Link*',
        'desc'     => 'A link to allow users to purchase tickets.',
        'id'       => $prefix . '_tickets_url',
        'required' => true,
        'type'     => 'text'
    ),
    array(
        'label'    => 'VIP Tickets Link',
        'desc'     => 'A link to allow users to purchase VIP tickets.',
        'id'       => $prefix . '_tickets_url_vip',
        'required' => false,
        'type'     => 'text'
    ),
    array(
        'label'    => 'Facebook Event ID',
        'desc'     => 'The Facebook Event ID will link users to the Facebook event. Ex: For facebook.com/events/433212686809974, the ID is 433212686809974.',
        'id'       => $prefix . '_facebook_id',
        'required' => false,
        'type'     => 'text'
    ),
);

require_once(EVENT_CPT_DIR.'/cron.php');
require_once(EVENT_CPT_DIR.'/event-functions.php');

add_action('add_meta_boxes', 'add_custom_meta_box');
add_action('save_post', 'save_custom_meta');
add_action('init', 'event_cpt', 0); // Hook into the 'init' action
add_action('init', 'event_cpt_scripts');
add_action('admin_init', 'event_cpt_admin_scripts');
add_shortcode('event-cpt', 'event_cpt_shortcode');
